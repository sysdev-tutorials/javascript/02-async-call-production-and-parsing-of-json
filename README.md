# Async-kall + production and parsing of JSON

We will learn the following main concepts in this session.
 - Review of Vuejs Application
 - Promise and asynchronous operations in Vuejs/JavaScript
 - Making api calls from Vuejs/JavaScript components


## Introduction to Vuejs + API call

### 1. Createing a Vuejs project
Make sure you have an up-to-date version of Node.js installed, then run the following command in your command line ```npm init vue@latest```. This command will install and execute create-vue, the official Vue project scaffolding tool.
You will then be asked various options and following snippet shows the the recommended options to use! Checkout generated ```README.md``` file for more details.

```
✔ Project name: … <your project>
✔ Add TypeScript? … Yes
✔ Add JSX Support? … Yes
✔ Add Vue Router for Single Page Application development? … Yes
✔ Add Pinia for state management? … Yes
✔ Add Vitest for Unit Testing? … Yes
✔ Add an End-to-End Testing Solution? › Cypress
✔ Add ESLint for code quality? … Yes
✔ Add Prettier for code formatting? … Yes

Scaffolding project in <your project location>...

Done. Now run:

  cd <your project>
  npm install
  npm run lint
  npm run dev
```

Run and inspect the generated app, especially understand the folling points
- the project structure
- how ```index.html``` in the root folder maps the main Vuejs component ```src/App.vue``` via ```src/main.ts``` file
- how to write reusable components! Check components in ```src/components``` folder 
- how to use reusable components! Check components in ```src/views``` folder
- how the routers and stores are setup! You will need these in your project later in the semester

Sample created project is located under ```01-introduction-to-vuejs``` folder.

### 2. Adding a simple login feature to the application
Now, lets add a LoginView to the application generated above. 
The behavior of LoginView is following. 
It will show username and password field and a login button. 
When username and password is provided and login button is pressed, an API call will be made to the server. 
If the server indicated successful login, the HomeView will be shown. 
Otherwise error messages will be shown in the LoginView itself.

So, we will need to do the following things
- add a login view ```views/LoginView.vue```. Note the a typical Vuejs component contains three parts - template, script and style. Understand the code in the ```LoginView.vue``` component - elements and event handlers!
  ```
  <template>
      <div id="loginContainer">
        <div id="loginTitle">
          <label>Please login!</label>
        </div>
        <div id="username">
          <label id="usernameLabel" id="username">Username:</label>
          <textarea v-model="username"></textarea>
        </div>
        <div id="password">
          <label id="passwordLabel">Password: </label>
          <textarea v-model="password" id="password"></textarea>
          <button v-on:click="handleLoginClick" id="signinbutton">Sign in</button>
          <label id="loginstatusLabel">{{ loginStatus }}</label>
        </div>
      </div>
    </template>
    
    <script lang="ts">
    import router from '@/router';
    export default {
      name: "LoginView",
      methods: {
        handleLoginClick() {
          router.push("/home");
        },
      },
      data() {
        return {
          username: "",
          password: "",
          loginStatus: "",
        };
      },
    };
    </script>
    
    <style scoped>
    #loginContainer {
      display: grid;
      justify-content: center;
      margin: 40px;
    }
    
    #loginTitle {
      font-size: x-large;
      font-weight: bold;
      margin-bottom: 20px;
    }
    
    #username,
    #password {
      display: flex;
      flex-direction: row;
      align-items: center;
      column-gap: 20px;
    }
    
    #usernameLabel,
    #passwordLabel {
      width: 100px;
    }
    </style>
  ```
- add a router ```/login``` that will bind to the ```LoginView``` by modifying ```src/router/index.ts``` such that the default path ```/``` is mapped to the new ```LoginView``` and ```HomeView``` mapping is changed to ```/home``` path.

  ```
  import { createRouter, createWebHistory } from 'vue-router'
  import HomeView from '../views/HomeView.vue'
  import LoginView from '../views/LoginView.vue'

  const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
      {
        path: '/home',
        name: 'home',
        component: HomeView
      },
      {
        path: '/',
        name: 'login',
        component: LoginView,
      },
      {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (About.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import('../views/AboutView.vue')
      }
    ]
  })

  export default router
  ```

- Last but not the least, update the router links menu in ```src/App.vue``` as below. Note that we have taken away router link for home screen (```<RouterLink to="/home">Home</RouterLink>```) as we will show the home screen if only if the login is successful!
  ```
  <RouterLink to="/about">About</RouterLink>
  <RouterLink to="/">Login</RouterLink>
  ```

Rerun and inspect the app!


### 3. Making api calls 
In the previous section we have created a Vuejs app and also added a simple login user-interface.

The goal for this section is to make use of API calls from UI. 
For example when user clicks ```Sign in``` button, an API call will be made to the server. 
Based on the response from the server, either ```HomeView``` or error will be shown.

That means we need a server offering login APIs. Lets start with this.

### 3.1 Run a simple demo API locally
You will learn how you can create such APIs in future lessons in this course, and today we will just simulate/mock it using a library called ```json-server```. More on it can be found here https://github.com/typicode/json-server

The json-server can be installed either global to the system or local to a project. 
In the following we will describe how it can be installed local to a project and then therafter use it!

- Installation can be done by running this command from the project directory

    ```​
    npm install --save-dev json-server​
    ```

    The above command will add 'json-server' as dev dependency for the project which can be verified by inspecting 'package.json' file.


- Then append this line ```"mockapi": "json-server -w data/dummydata.json"``` to the scripts section in 'package.json' file so that the json-server can be started using ```npm run mockapi``` command​. 
Note that we have used 'data/dummydata.json' as mock data source for json-server. The sample content for the dummydata.json file is as following. 
   
   ```
   // contents for data/dummydata.json file
   {
    "login": {
      "status": "true"
    }
  }
   ```

- Now start the json server using ```npm run mockapi``` command from project root directory​. It should be ready to accept requests! Verify that server is running fine by doing a HTTP GET call using curl or postman or other tools as below.

  ```
  // curl request
  curl localhost:3000/login

  // api response
  {
    "status": "true"
  }
  ```

So, we have now API ready to be used, for example by the user-interface.

### 3.2 Call an API on ```Sign in``` button click
In order to do API calls, we will use a third party library called ```Axios```, ref https://github.com/axios/axios. One can also use a library called ```fetch``` if that is preferred.

1. Add library to the project. Go to the project home folder and execute the following command in the terminal.

    ```
    npm install axios
    ```

2. Verify that ```Axios``` is`added to the project by inspecting  ```dependencies``` section in ```package.json``` file .
    ```
    ...
      "dependencies": {
        "axios": "1.1.3",
        ...
      },
      ...
    ```  
3. Import  ```Axios ``` in the  ```views/LoginView.vue```
    ```
    import axios from 'axios';
    ```
4. Modify the ```handleLoginClick``` method
    ```
      ...
      handleLoginClick () {
        const loginResponse = axios.get("http://localhost:3000/login");
        alert("Login: " + loginResponse);
      }
      ...
    ```
5. Run the app. Type random ```username``` and ```password``` in the username and password input fields, and click ```Sign in``` button. See what happens? Did it meet your expectation? Well, unexpectedly, you get 'Promise' as alert or output! So, we will explore more about ```Promise``` next!!

### 3.3 Promise and asynchronous operation
Usually, we expect that when a function is called, it will execute all the statements within it and immediately return back control or result i.e. synchronously. This is synchronous nature of function calls. 

But sometimes function contains potentially long-running task and if one has to wait until that task has finished, the application will not be non-responsive. 

Asynchronous programming technique enables your program to start a potentially long-running task, still be responsive, and process the result when it is later available.

Let´s understand synchronous and asynchronous behavior with a example.
Lets create ```SyncAsyncExample``` component under ```src/components``` folder. 

```
<script>
export default {
  name: 'SyncAsyncExample',
  data () {
    return {}
  },
  methods: {
    printABC_sync(){
      console.log('A');
      console.log('B');
      console.log('C');
    },
    printABC_async(){
      console.log('A1');
      setTimeout(function(){
        console.log('B1')
      }, 1000);
      console.log('C1')
    }
  },
  mounted() {
    this.printABC_sync();
    this.printABC_async();
  }
}
</script>

<template></template>
```

Note that the component has two functions ```printABC_sync``` and ```printABC_async```. If you import and use this component for example in ```LoginView.vue``` and inspect the console log, you will see the following output. Note especially that C1 is printed becore B1! 

```
A
B
C

A1
C1
B1
```

Here timeout feature is the cause of asynchronocity. In real life scenario, it can be a network call, a long running operation and so on.

In the above function, the setTimeout function takes two parameters. The first parameter is a function which is called after the time elapsed specified in the second parameter. Such functions are called ```Callback function```. 

Note that this callback function can be defined separately and used as below resulting same effect i.e. printing A1, C1 and B1 in order.

```
    print_B1(){
      console.log('B1');
    }
    printABC_async(){
      console.log('A1');
      setTimeout(this.print_B1, 1000);
      console.log('C1')
    }
```

Note that callback based async code is difficult to read and maintain when there are multiple nested async operations. Check ```callback hell``` problem! Nice read here, https://www.freecodecamp.org/news/how-to-deal-with-nested-callbacks-and-avoid-callback-hell-1bc8dc4a2012/

In the following, we will learn ```Promise``` based alternative!

So, lets rewrite the above async function using Promise, without callback function, resulting same effect i.e. printing A1, C1 and B1 in order.!

```
    print_B1(){
      return new Promise((resolve) => {
        setTimeout(resolve, 1000);
      });
    }
    printABC_async(){
      console.log('A1');
      this.print_B1().then(()=> {
        console.log('B1')
      });
      console.log('C1')
    }
```

Note the usage of ```Promise``` above! Promise is a future object, which can be resolved or rejected in the future. In the above example, we just provided resolve function to the setTimeout. And, in printABC_async, we printed B1 when the promise is resolved using ```then``` keyword/function!

How about if you want to do SOMETHING (for ex. print B after 1 sec timeout) before resolving the Promise? Thats possible, for example like this below. Note that we will still get the same result i.e. printing A1, C1 and B1 in order. But the point here was to show different possibilities of using Promise object - ex, doing something before resolving promise, after promise is resolved and so on!

```
    print_B1(){
      return new Promise((resolve) => {
        setTimeout(() => {
          // do whatever here!!
          console.log('B1');
          resolve();
        }, 1000);
      });
    },
    printABC_async(){
      console.log('A1');
      this.print_B1();
      console.log('C1')
    }
```

So far, we have focused on the asynchronous behavior - using both callback function and Promises. Note that with Promise, async operations can be made ```synchronous``` using keywords ```async``` and ```await```, like below. Note that this will print A1, B1 and C1 in order (unlike before!)

```
    async printABC_async(){
      console.log('A1');
      await this.print_B1();
      console.log('C1')
    }
```

How does ```input and outputs``` work with Promise? Lets extend the above example - for example by sending timeout as input parameter and returning a string output from the async function. Inspect the output and it will print A1, B1 and C1 in order (as before!)

```
    print_B1(timeout){
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve("B1");
        }, timeout);
      });
    },
    async printABC_async(){
      console.log('A1');
      await this.print_B1(1000).then(data => {
        console.log(data);
      });
      console.log('C1')
    }
```
### 3.4 Back to API calls
Earlier, we have seen that result from ```axios.post``` call was a Promise object. Now we understand what a ```Promise``` is. 

That means we have two options to resolve Promise object - either use ```then``` pattern or use ```async``` and ```await```. Choose per your preference! The later patter gives better code readibility and maintainability. 

Let us first update the handleLoginClick function using ```async``` and ```await```as following. Then, if you retun the application, a ```Login``` object will be alerted, instead of Promise object.

```
  async handleLoginClick () {
    const loginResponse = await axios.get("http://localhost:3000/login");
    alert("Login: " + loginResponse);
  }
```

Then replace ```alert``` statement with a check - if login response contains a data that indicates if login is successful or not! If login is successful, redirect to ```/home``` page

```
  async handleLoginClick () {
    const loginResponse = await axios.get("http://localhost:3000/login");
    if(loginResponse.data.status == "true"){
      router.push("/home");
    }
  }
```

Note also that same effect can be achieved using ```then``` pattern, like this!
```
  handleLoginClick() {
    axios.get("http://localhost:3000/login").then(response => {
      if(response.data.status == "true"){
        router.push("/home");
      }
    })
  }
```

Note that we have so far used only ```GET```method on axios. It is however possible to use other http methods such as ```POST```, ```PUT```, ```DELETE``` and so on. 
More on axios can be read here https://axios-http.com/docs/intro


### 3.5 Notes on Json parsing
If you debug and check the ```response``` from axios calls, it contains couple of properties -  for example ```data```, ```status```, ```statusText``` etc. The ```data``` property is simply the response provided by the server. It can be anytype of data provided by the server. In our case, it is json type provided by json-server on ```GET``` at ```/login``` endpoint.

SO, when we have json object, we can use regular JavaScript to manipulate (or work on) json objects - for example using dot(.) notation and indexes to access objects and object properties!

Note that by default, fetch accepts ```json``` datatype. If the server provides different datatype, it can be specified while doing a rest call, for example as request configuration parameter!


### 3.6 Refactoring
Let´s do small refactoring of the code we have written above!

First, it would be natural to have API call related code in one place. For example all different types of network call be be placed in to one place so that they can potentially be reused! For that create a ```utils/httputils.js``` file with following contents.

```
// contents of utils/httputils.js file
import axios from "axios";

const baseurl = "http://localhost:3000";

export const loginRequest = (user) => {
    const config = {
        headers: {
            "Content-type": "application/json",
        },
    };
    return axios.get(baseurl + "/login", config);
}
```

Next, create another util file ```utils/loginutils.js``` that contains business logic related to user login. Currently we will have only one method that checks if the user login is successful or not. Later additional functionalities can be added! The contents of this file is following.

```
// contents of utils/loginutils.js file

export function isLoginSuccessful (response) {
    if (response == undefined || response == null ) return false;
    if (response.data == undefined || response.data == null ) return false;
    return response.data.status == "true";
}
```

Then, these utilities functions can be imported into ```LoginView``` 
```
  //in LoginView.vue file
  import {isLoginSuccessful} from "../utils/loginutils.js"
  import {loginRequest} from "../utils/httputils.js"
```

And can be used inside ```handleLoginClick``` function as follows,

```
  // in LoginView.vue file
  handleLoginClick() {
    loginRequest({}).then(response => {
      if(isLoginSuccessful(response)){
        router.push("/home");
      }
    })
  },
```

Note that it can also be used inside ```handleLoginClick``` function using async/await pattern as follows,
```
  // in LoginView.vue file
  async handleLoginClick() {
    let response = await loginRequest({})
    if(isLoginSuccessful(response)){
      router.push("/home");
    }
  }
```